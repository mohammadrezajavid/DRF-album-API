#!/bin/bash
DIR_PATH="/home/ubuntu/var/app/gitlab/"
APP_PATH="/home/ubuntu/var/app/gitlab/DRF-album-API/"

if [ -d $APP_PATH ]
then
        cd $APP_PATH
        git pull origin master
else
        mkdir -p $DIR_PATH && cd $DIR_PATH
        git clone https://gitlab.com/mohammadrezajavid/DRF-album-API.git
fi

cd $APP_PATH

python3 -m venv env
source env/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r ./requirements.txt
python3 manage.py makemigrations && python manage.py migrate --database=default
#python3 manage.py test
#python3 manage.py runserver > /dev/null 2>&1 &
sudo systemctl restart gunicorn