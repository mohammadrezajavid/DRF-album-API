[//]: # (## How to use the code)

[//]: # ()

[//]: # (***Clone the sources***)

[//]: # ()

[//]: # (```bash)

[//]: # ($ git clone https://github.com/MohammadrezaJavid/DRF-album-API.git)

[//]: # ($ cd DRF-album-API)

[//]: # (```)

[//]: # ()

[//]: # (***Config env file***)

[//]: # (```markdown)

[//]: # (First, create the env.py file similar to the example_env.py)

[//]: # (and define the appropriate values for the variables of this file.)

[//]: # (```)

[//]: # ()

[//]: # (***Create a virtual environment***)

[//]: # ()

[//]: # (```bash)

[//]: # ($ make create-venv)

[//]: # (```)

[//]: # ()

[//]: # (***Install dependencies*** using pip)

[//]: # ()

[//]: # (```bash)

[//]: # ($ make install-requirements)

[//]: # (```)

[//]: # ()

[//]: # (***Migrate database***)

[//]: # ()

[//]: # (```bash)

[//]: # ($ make migrate-database)

[//]: # (```)

[//]: # ()

[//]: # (***Run tests API***)

[//]: # ()

[//]: # (```bash)

[//]: # ($ make run-tests)

[//]: # (```)

[//]: # ()

[//]: # (***Start the API server***)

[//]: # ()

[//]: # (```bash)

[//]: # ($ make run-server)

[//]: # (```)

[//]: # ()

[//]: # (The API server will start using the default port `8000`.)

[//]: # ()

[//]: # (<br />)

***This api urls***

```markdown

1. https://api.jlinux.ir/auth/register/
2. https://api.jlinux.ir/auth/login/
3. https://api.jlinux.ir/auth/refresh/
4. https://api.jlinux.ir/api/v1/albums/
```

***And just for fun, check root domain***
*https://jlinux.ir*

<p align="center">
  * * *
</p>

**To implement this project, you can follow the steps below**

1. Create a VPS and do the necessary configurations to connect to it.
   These configurations can include creating an ssh-key for easy connection to the server.

---

2. Pull the project from the [link](https://gitlab.com/mohammadrezajavid/DRF-album-API) below and create a repo in
   gitlab and put the project in it

---

3. Install and configure the postgresql database
   ```bash 
   # install postgres
   $ sudo apt-get update
   $ sudo apt-get install postgresql postgresql-contrib
   
   # config database
   $ sudo -u postgres psql
   postgres=# CREATE DATABASE album;
   postgres=# CREATE USER usertest WITH PASSWORD 'password';
   postgres=# GRANT ALL PRIVILEGES ON DATABASE album TO usertest;
   ```

---

4. Now enter the project directory named DRF-album-API

    - Create a file called .env and enter the necessary variables to connect to the database according to your needs,
      similar to the following file
       ```markdown
         SECRET_KEY=<secret_key>
         DB_NAME=<database_name>
         DB_USER=<database_user>
         DB_PASSWORD=<database_password>
         DB_HOST=<database_host>
         DB_PORT=<database_port>
       ```
    And do the following commands in order to apply the settings related to the project.
   ```bash
   $ make create-venv
   $ . ./env/bin/activate
   $ make update-pip
   $ make install-requirements
   $ make migrate-database
   ```

---

5. set variables for gitlab pipeline
TODO

---

6. configure gunicorn
TODO

---

7. install and configure nginx
TODO

---

8. install and configure certbot for https

TODO
